<?php

	class Parse{
		
		function isValid($MSISDN) {
			
			if(preg_match("/^\+[1-9]{1}[0-9]{3,14}$/",$MSISDN) == 1){
				$info = array("status" => "Entered MSISDN is valid!", "data" => $this->parseMSISDN($MSISDN));	
			}else{
				$info = array("status" => "Entered MSISDN is not valid!", "data" => array($country = "unknown country",
																						  $dialcode = "unknown country dialing code",
																						  $mno = "unknowm MNO",
																						  $sn = "unknown"));
			}
			
			return json_encode($info);
		}
		
		private function parseMSISDN($MSISDN){
			$short = substr($MSISDN, 1, 1);
			$medium = substr($MSISDN, 1, 2);
			$long = substr($MSISDN, 1, 3); 
			
			//default info
			$country = "unknown country";
			$dialcode = "unknown country dialing code";
			$mno = "unknowm MNO";
			$sn = "unknown";
			
			$jsondata = file_get_contents("country.json");
			$data = json_decode($jsondata, true);
			$jsondata = file_get_contents("SLO.json"); //trenutno samo podatki o slovenskih operaterjih
			$data2 = json_decode($jsondata, true);
			
			if(array_key_exists($long, $data)){
				$country = $data[$long];
				if(0 != strcmp($country, "SI"))
					goto end;
				$dialcode = $long;
				$mno = array_key_exists(substr($MSISDN, 4, 2), $data2) ? $data2[substr($MSISDN, 4, 2)] : "unknown MNO";
				$sn = substr($MSISDN, 4);
			}else if(array_key_exists($medium, $data)){
				$country = $data[$medium];
				if(0 != strcmp($country, "SI"))
					goto end;
				$dialcode = $medium;
				$mno = array_key_exists(substr($MSISDN, 3, 2), $data2) ? $data2[substr($MSISDN, 3, 2)] : "unknown MNO";
				$sn = substr($MSISDN, 3);
			}else if(array_key_exists($short, $data)){
				$country = $data[$short];
				if(0 != strcmp($country, "SI"))
					goto end;
				$dialcode = $short;
				$mno = array_key_exists(substr($MSISDN, 2, 2), $data2) ? $data2[substr($MSISDN, 2, 2)] : "unknown MNO";
				$sn = substr($MSISDN, 2);
			}
			
			end:
			return array($country, $dialcode, $mno, $sn);
			
		}
		
	}
	
	
?>
